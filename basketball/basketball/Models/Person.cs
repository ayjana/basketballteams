﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace basketball.Models
{
    public abstract class Person : IEntity<int>
    {
        [ForeignKey("Team")]
        public int? TeamID { get; set; }
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public virtual Team Team { get; set; }

    }
}
