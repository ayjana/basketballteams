﻿using System;
using System.Collections.Generic;
using System.Text;

namespace basketball.Models
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
