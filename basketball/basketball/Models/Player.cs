﻿using basketball.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace basketball.Models
{
    public class Player : Person
    {
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public int TShirtNumber { get; set; }
        public Position Position { get; set; }
    }
}
