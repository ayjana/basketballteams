﻿using basketball.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace basketball.Models
{
    public class Team : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Coach")]
        public int? CoachID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string City { get; set; }
        public Conference Conference { get; set; }
        public DateTime FundYear { get; set; }
        public int Victories { get; set; }
        public int Defeats { get; set; }
        public virtual ICollection<Player> Players { get; set; }
        public virtual Coach Coach { get; set; }
    }
}
