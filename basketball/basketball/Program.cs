﻿using basketball.Models;
using System;
using System.Linq;

namespace basketball
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }
        public static void Seed(ApplicationDbContext db)
        {
            if (!db.Persons.Any())
            {
                var player1 = new Player
                {
                    ID = 1,
                    Name = "A",
                    Surname = "K",
                    DateOfBirth = new DateTime(2002, 7, 29),
                    TeamID = 1,
                    Height = 123,
                    Weight = 456,
                    TShirtNumber = 1,
                    Position = (Enums.Position)1
                };



                var coach = new Coach
                {
                    ID = 1,
                    Name = "B",
                    Surname = "C",
                    DateOfBirth = new DateTime(2002, 8, 29),
                    TeamID = 1,
                    YearsOfExperience = 6
                };



                db.Persons.Add(coach);
                db.Persons.Add(player1);
                db.SaveChanges();
            }
            if (!db.Teams.Any())
            {
                var team1 = new Team
                {
                    ID = 1,
                    Conference = (Enums.Conference)1,
                    FundYear = new DateTime(2000, 8, 29),
                    Victories = 1,
                    Defeats = 0
                };

                db.Teams.Add(team1);
                db.SaveChanges();
            }
        }
    }
}
