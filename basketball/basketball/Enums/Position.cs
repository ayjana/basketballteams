﻿using System;
using System.Collections.Generic;
using System.Text;

namespace basketball.Enums
{
    public enum Position
    {
        PG = 1, 
        SG = 2,
        SF = 3,
        PF = 4,
        C = 5
    }
}
